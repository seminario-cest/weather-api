import sqlite3
from contextlib import closing

connection = sqlite3.connect('weather.db')

with closing(connection) as db:
	with open('seed.sql', mode='r') as f:
			db.cursor().executescript(f.read())
	db.commit()
