from flask import Flask
from weather_api.api.weather import weather_bs

app = Flask(__name__)

app.register_blueprint(weather_bs)
