from datetime import date

from flask import Blueprint, request, json

from sqlalchemy import create_engine
from sqlalchemy import select
from sqlalchemy.orm import Session

from weather_api.domain.weather import Weather

weather_bs = Blueprint("weather", __name__)

engine = create_engine("sqlite:///weather.db", echo=True)


@weather_bs.route("/weather", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        data = json.loads(request.data)

        if data["meantemp"] == "":
            return "O campo 'meantemp' é obrigatório", 400

        if data["humidity"] == "":
            return "O campo 'humidity' é obrigatório", 400

        if data["wind_speed"] == "":
            return "O campo 'meantemp' é obrigatório", 400

        if data["meanpressure"] == "":
            return "O campo 'meantemp' é obrigatório", 400

        with Session(engine) as session:
            new_weather_info = Weather(
                date=date.today(),
                meantemp=data["meantemp"],
                humidity=data["humidity"],
                wind_speed=data["wind_speed"],
                meanpressure=data["meanpressure"],
            )

            session.add(new_weather_info)

            session.commit()

            return "", 201
    else:
        with Session(engine) as session:
            stmt = select(Weather)

            return [
                {
                    "id": weather.id,
                    "meantemp": weather.meantemp,
                    "humidity": weather.humidity,
                    "wind_speed": weather.wind_speed,
                    "meanpressure": weather.meanpressure,
                }
                for weather in session.scalars(stmt)
            ]
