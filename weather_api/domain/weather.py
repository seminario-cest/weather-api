from sqlalchemy import String
from sqlalchemy import Date
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column


class Base(DeclarativeBase):
    pass


class Weather(Base):
    __tablename__ = "weather"

    id: Mapped[int] = mapped_column(primary_key=True)
    date: Mapped[date] = mapped_column(Date)
    meantemp: Mapped[str] = mapped_column(String(18))
    humidity: Mapped[str] = mapped_column(String(18))
    wind_speed: Mapped[str] = mapped_column(String(18))
    meanpressure: Mapped[str] = mapped_column(String(19))
